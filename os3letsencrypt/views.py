"""
Copyright 2016-2017 Peter Urda

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.http import Http404
from .forms import AcmeChallengeForm
from .models import AcmeChallenge
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt


def detail(request, acme_data):
    acme_challenge = get_object_or_404(
        AcmeChallenge,
        challenge=acme_data,
    )

    return HttpResponse(acme_challenge.response, content_type="text/plain")


@csrf_exempt
def post(request):
    if request.META.get('HTTP_TOKEN', '') != getattr(settings, 'ACME_TOKEN', ''):
        raise Http404

    if request.POST.get('challenge', ''):
        AcmeChallenge.objects.filter(challenge=request.POST['challenge']).delete()
    form = AcmeChallengeForm(request.POST)
    if form.is_valid():
        form.save()
    else:
        raise Http404

    return HttpResponse()