from django import forms
from .models import AcmeChallenge


class AcmeChallengeForm(forms.ModelForm):
    class Meta:
        model = AcmeChallenge
        fields = '__all__'