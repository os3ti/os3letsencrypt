from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcmeChallenge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('challenge', models.CharField(help_text='The identifier for this challenge', max_length=255, unique=True)),
                ('response', models.CharField(help_text='The response expected for this challenge', max_length=255)),
            ],
            options={
                'verbose_name': 'ACME Challenge',
                'verbose_name_plural': 'ACME Challenges',
            },
        ),
        migrations.CreateModel(
            name='NextCheck',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('expires_on', models.DateField()),
                ('site', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
        ),
    ]
