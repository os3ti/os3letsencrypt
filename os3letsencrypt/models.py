from django.db import models
from django.urls import (
    reverse,
    NoReverseMatch,
)


class AcmeChallenge(models.Model):
    challenge = models.CharField(
        help_text='The identifier for this challenge',
        unique=True,
        max_length=255,
    )

    response = models.CharField(
        help_text='The response expected for this challenge',
        max_length=255,
    )

    def __str__(self):
        return self.challenge

    def get_acme_url(self):
        try:
            return reverse(
                viewname='detail',
                current_app='letsencrypt',
                args=[self.challenge],
            )
        except NoReverseMatch:
            return ''

    class Meta:
        verbose_name = 'ACME Challenge'
        verbose_name_plural = 'ACME Challenges'
