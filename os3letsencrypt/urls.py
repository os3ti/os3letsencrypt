from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^acme-challenge/(?P<acme_data>.+)$', views.detail, name='os3letsencrypt_detail'),
    url(r'^acme-challenge/$', views.post, name='os3letsencrypt_post'),
]
