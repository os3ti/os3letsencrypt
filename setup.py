import os
from setuptools import setup

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='os3letsencrypt',
    version='1.0',
    python_requires=">=2.7, <4",
    packages=['os3letsencrypt'],
    include_package_data=True,
    install_requires=[
        "Django>=1.10.0",
    ],

)
# git push -u origin master